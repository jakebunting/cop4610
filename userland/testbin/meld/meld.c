/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * meld.c
 *
 * 	Tests sys_meld by providing parameters, invoking the system call, and
 *	checking the return value and results.
 *
 * This should run (on SFS) even before the file system assignment is started.
 * It should also continue to work once said assignment is complete.
 * It will not run fully on emufs, because emufs does not support remove().
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <err.h>

int
main(int argc, char *argv[])
{
	static char writebufa[12] = "AAAABBBBCCCC";
	static char writebufb[12] = "ddddeeeeffff";
	static char readbuf[25];

	const char *file1, *file2, *meldFile;
	int fd, rv, mf = 0;

	if (argc == 2) {
		file1 = argv[1];
	}
	else if (argc > 2){
		errx(1, "Usage: testbin <filename>");
	}
	file1 = "file1";
	file2 = "file2";
	meldFile = "meldFile";

	fd = open(file1, O_WRONLY|O_CREAT|O_TRUNC, 0664);
	if (fd<0) {
		err(1, "%s: open for write", file1);
	}


	rv = write(fd, writebufa, 12);
	if (rv<0) {
		err(1, "%s: write", file1);
	}

	rv = close(fd);
	if (rv<0) {
		err(1, "%s: close (1st time)", file1);
	}

	fd = open(file2, O_WRONLY|O_CREAT|O_TRUNC, 0664);
	if (fd<0) {
		err(1, "%s: open for write", file2);
	}


	rv = write(fd, writebufb, 12);
	if (rv<0) {
		err(1, "%s: write", file2);
	}

	rv = close(fd);
	if (rv<0) {
		err(1, "%s: close (2nd time)", file2);
	}

	meld(file1, file2, meldFile);

	fd = open(meldFile, O_RDONLY);
	if (fd<0) {
		err(1, "%s: open for read", meldFile);
	}

	rv = read(fd, readbuf, 24);
	if (rv<0){
		err(1, "%s: read", meldFile);
	}

	rv = close(fd);
	printf("rv = %d", mf);
	if (rv<0) {
		err(1, "%s: close (meld)", meldFile);
	}

	printf("\nPassed meldtest.\n");
	return 0;
	//(void) mf;
}
