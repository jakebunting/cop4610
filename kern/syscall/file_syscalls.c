/*
 * File-related system call implementations.
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <lib.h>
#include <uio.h>
#include <proc.h>
#include <current.h>
#include <synch.h>
#include <copyinout.h>
#include <vfs.h>
#include <vnode.h>
#include <openfile.h>
#include <filetable.h>
#include <syscall.h>

/*
 * open() - get the path with copyinstr, then use openfile_open and
 * filetable_place to do the real work.
 */
int
sys_open(const_userptr_t upath, int flags, mode_t mode, int *retval)
{
	const int allflags = O_ACCMODE | O_CREAT | O_EXCL | O_TRUNC | O_APPEND | O_NOCTTY;

	
	char *kpath;
	struct openfile *file;
	int result = 0;
	kpath = (char*)kmalloc(sizeof(char)*PATH_MAX);
	
	/* 
	 * Your implementation of system call open starts here.  
	 *
	 * Check the design document design/filesyscall.txt for the steps
	 */

	if(flags == allflags)//check flags
		return EINVAL;

	//copy in the supplied pathname
	result = copyinstr(upath, kpath, PATH_MAX, NULL);
	if(result != 0) 
		return EFAULT;

	//open the file (use openfile_open)
	result = openfile_open(kpath, flags, mode, &file);
	if(result != 0)
		return EFAULT;

	//place the file into curproc's file table (use filetable_place)
	result = filetable_place(curproc->p_filetable, file, retval);
	if(result != 0)
		return EMFILE;

	kfree(kpath);

	/* 
	 * Your implementation of system call open starts here.  
	 *
	 * Check the design document design/filesyscall.txt for the steps
	 */
	//(void) upath; // suppress compilation warning until code gets written
	//(void) flags; // suppress compilation warning until code gets written
	//(void) mode; // suppress compilation warning until code gets written
	//(void) retval; // suppress compilation warning until code gets written
	//(void) allflags; // suppress compilation warning until code gets written
	//(void) kpath; // suppress compilation warning until code gets written
	//(void) file; // suppress compilation warning until code gets written

	return result;
}

/*
 * read() - read data from a file
 */
int
sys_read(int fd, userptr_t buf, size_t size, int *retval)
{
	int result = 0;
	struct iovec iovec;
	struct uio useruio;
	struct openfile *file;
	/* 
        * Your implementation of system call read starts here.  
        *
        * Check the design document design/filesyscall.txt for the steps
	*/

	//translate the file descriptor number to an open file object (use filetable_get)
	result = filetable_get(curproc->p_filetable, fd, &file);

	if(result != 0)
		return result;

	//lock the seek position in the open file (but only for seekable objects)
	lock_acquire(file->of_offsetlock);

	//check for files opened write-only
	if(file->of_accmode == O_WRONLY){
		lock_release(file->of_offsetlock);
		return EACCES;
	}

	//construct a struct: uio
	uio_kinit(&iovec, &useruio, buf, size, file->of_offset, UIO_READ);
	
	//call VOP_READ
	result = VOP_READ(file->of_vnode, &useruio);

	if(result != 0)
		return result;

	//update the seek position afterwards
	file->of_offset = useruio.uio_offset;
	
	//unlock and filetable_put()
	lock_release(file->of_offsetlock);
	filetable_put(curproc->p_filetable, fd, file);

	//set the return value correctly
	*retval = size - useruio.uio_resid;
	
	

       /* 
        * Your implementation of system call read starts here.  
        *
        * Check the design document design/filesyscall.txt for the steps
        */
     //  (void) fd; // suppress compilation warning until code gets written
      // (void) buf; // suppress compilation warning until code gets written
      // (void) size; // suppress compilation warning until code gets written
       //(void) retval; // suppress compilation warning until code gets written

       return result;
}
/*
 * write() - write data to a file
 */
int 
sys_write(int fd, userptr_t buf, size_t size, int *retval)
{

	int result = 0;
	struct openfile *file;
	struct iovec iovec;
	struct uio useruio;
	
	//translate the file descriptor number to an open file object (use filetable_get)
	result = filetable_get(curproc->p_filetable, fd, &file);

	if(result != 0)
		return result;

	//lock the seek position in the open file (but only for seekable objects)
	lock_acquire(file->of_offsetlock);

	//check for files opened read-only
	if(file->of_accmode == O_RDONLY){
		lock_release(file->of_offsetlock);
		return EACCES;
	}
	//construct a struct: uio
	uio_kinit(&iovec, &useruio, buf, size, file->of_offset, UIO_WRITE);

	//call VOP_WRITE
	result = VOP_WRITE(file->of_vnode, &useruio);
	if(result != 0)
		return result;

	//update the seek position afterwards
	file->of_offset = useruio.uio_offset;

	//unlock and filetable_put()
	lock_release(file->of_offsetlock);
	filetable_put(curproc->p_filetable, fd, file);

	//set the return value correctly
	*retval = size - useruio.uio_resid;
	
	return result;
}
/*
 * close() - remove from the file table.
 */
int 
sys_close(int fd)
{
	struct openfile *file;

	//validate the fd number (use filetable_okfd)	
	KASSERT(filetable_okfd(curproc->p_filetable, fd));

	//use filetable_placeat to replace curproc's file table entry with NULL	
	filetable_placeat(curproc->p_filetable, NULL, fd, &file);

	//check if the previous entry in the file table was also NULL (this means no such file was open)
	if(file == NULL)
		return ENOENT;
	else{
	//decref the open file returned by filetable_placeat
		openfile_decref(file);
	}

	return 0;
	
}
/* 
* meld () - combine the content of two files word by word into a new file
*/
int sys_meld(const_userptr_t pn1, const_userptr_t pn2, const_userptr_t pn3, int *retval)
{
	struct openfile *file1, *file2, *meldFile;

	int result = 0, count = 0, fd, temp, size;
	char *kpath1 = (char*)kmalloc(sizeof(char)*PATH_MAX);
	char *kpath2 = (char*)kmalloc(sizeof(char)*PATH_MAX);
	char *kpath3 = (char*)kmalloc(sizeof(char)*PATH_MAX);
	
	struct iovec iovec;
	struct uio useruiof1, useruiof2, writeuio;
	struct stat status;

	//4 byte buffers for file1 and file2
	char *buf1 = (char*)kmalloc(sizeof(char)*4);
	char *buf2 = (char*)kmalloc(sizeof(char)*4);

	//copy in the supplied pathnames (pn1, pn2, and pn3)
	result = copyinstr(pn1, kpath1, PATH_MAX, NULL);
	if(result != 0)
		return result;
	result = copyinstr(pn2, kpath2, PATH_MAX, NULL);
	if(result != 0)
		return result;
	result = copyinstr(pn3, kpath3, PATH_MAX, NULL);
	if(result != 0)
		return result;

	//open the first two files (use openfile_open) for reading
	result = openfile_open(kpath1, O_RDWR, 0664, &file1);
	if(result != 0)	//return if any file is not open'ed correctly
		return ENOENT;
	result = openfile_open(kpath2, O_RDWR, 0664, &file2);
	if(result != 0)	//return if any file is not open'ed correctly
		return ENOENT;

	//open the third file (use openfile_open) for writing
	result = openfile_open(kpath3, O_WRONLY | O_CREAT | O_EXCL, 0664, &meldFile);
	if(result != 0)	//return if any file is not open'ed correctly
		return EEXIST;
	
	//place them into curproc's file table (use filetable_place) 
	result = filetable_place(curproc->p_filetable, file1, &temp);
	if(result != 0)
		return result;
	result = filetable_place(curproc->p_filetable, file2, &temp);
	if(result != 0)
		return result;
	result = filetable_place(curproc->p_filetable, meldFile,&temp);
	if(result != 0)
		return result;
	fd = temp;

	//get size of files
	result = VOP_STAT(file1->of_vnode, &status);
	size = status.st_size;
	result = VOP_STAT(file2->of_vnode, &status);
	size = size + status.st_size;

	

	while(count < size/2){
		//files read 4 bytes
		lock_acquire(file1->of_offsetlock);
		uio_kinit(&iovec, &useruiof1, buf1, 4, file1->of_offset, UIO_READ);
		result = VOP_READ(file1->of_vnode, &useruiof1);
		if(result != 0)
			return result;
		file1->of_offset = useruiof1.uio_offset;
		lock_release(file1->of_offsetlock);
		
		lock_acquire(file2->of_offsetlock);
		uio_kinit(&iovec, &useruiof2, buf2, 4, file2->of_offset, UIO_READ);
		result = VOP_READ(file2->of_vnode, &useruiof2);
		if(result != 0)
			return result;
		file2->of_offset = useruiof2.uio_offset;
		
		lock_release(file2->of_offsetlock);

		//4 bytes written from each file to meld file
		lock_acquire(meldFile->of_offsetlock);
		uio_kinit(&iovec, &writeuio, buf1, 4, meldFile->of_offset, UIO_WRITE);
		result = VOP_WRITE(meldFile->of_vnode, &writeuio);
		if(result != 0)
			return result;
		meldFile->of_offset = writeuio.uio_offset;
		
		lock_release(meldFile->of_offsetlock);

		lock_acquire(meldFile->of_offsetlock);
		uio_kinit(&iovec, &writeuio, buf2, 4, meldFile->of_offset, UIO_WRITE);
		result = VOP_WRITE(meldFile->of_vnode, &writeuio);
		if(result != 0)
			return result;
		meldFile->of_offset = writeuio.uio_offset;
		lock_release(meldFile->of_offsetlock);
	
		//increment count 4 bytes
		count = count + 4;
	}

	*retval = meldFile->of_offset;
	
	//close the files

	KASSERT(filetable_okfd(curproc->p_filetable, fd));

	filetable_placeat(curproc->p_filetable, NULL, fd, &file1);

	filetable_placeat(curproc->p_filetable, NULL, fd, &file2);
	
	filetable_placeat(curproc->p_filetable, NULL, fd, &meldFile);

	if(file1 == NULL)
		return ENOENT;
	if(file2 == NULL)
		return ENOENT;
	if(meldFile == NULL)
		return ENOENT;

	openfile_decref(file1);
	openfile_decref(file2);
	openfile_decref(meldFile);
	
	kfree(kpath1);
	kfree(kpath2);
	kfree(kpath3);
	kfree(buf1);
	kfree(buf2);
	return 0;
}

